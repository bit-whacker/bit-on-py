import csv
import json
import sys
from timeit import itertools

import tweepy


#http://www.tweepy.org/
class DataDumper():
    
    consumer_key = "***"
    consumer_secret = "***"
    access_key = "***"
    access_secret = "***"
    api = ".."
    def __init__(self):
        self.consumer_key = "**your consumer key here**"
        self.consumer_secret = "**your consumer secret key here**"
        self.access_key = "**your access key here**"
        self.access_secret = "**your access key secret here**"

        auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        auth.set_access_token(self.access_key, self.access_secret)
        self.api = tweepy.API(auth)
    
    def dumpTimeline(self, twitterUser):
        
        number_of_tweets = 2

        tweets = self.api.user_timeline(screen_name = twitterUser,count = number_of_tweets)
        
        tweets_for_csv = [[twitterUser,tweet.id_str, tweet.created_at, tweet.text.encode("utf-8")] for tweet in tweets]
        
        print "writing to {0}_tweets.csv".format(twitterUser)
        with open("{0}_tweets.csv".format(twitterUser) , 'w+') as file:
            writer = csv.writer(file, delimiter='|')
            writer.writerows(tweets_for_csv)
            
    def dumpTweets(self, searchKeyword, limit):
        tweets_collection = []
        allTweets = []
        try:
            tweets_collection = self.api.search(q = searchKeyword, count=limit, lang= 'en')
            print "downloading tweets...."
            while len(tweets_collection) < limit:
                print 'inside while'
                allTweets = []
                allTweets = self.api.search(q = searchKeyword, count=100, lang= 'en')
                for tweet in allTweets:
                    tweets_collection.append(tweet._json)
                    
                print "downloaded tweets: " + str(len(tweets_collection)) + "\n"
                
        except tweepy.TweepError, e:
            print "Exception occured!!!"
            print e
            if str(e) == str([{u'message': u'Bad Authentication data.', u'code': 215}]):
                return e
            elif str(e) == str([{u'message': u'Invalid or expired token.', u'code': 89}]):
                return e
            elif str(e) == "Not authorized.":
                return e
            else:
                return e
            
        #with open('iphone_tweets.txt', "a", encoding='utf8') as outfile:
        tweets = {};
        tweets["all_tweets"] = tweets_collection
            
        with open('iphone_tweets.txt', "a") as outfile:
            json.dumps(tweets, outfile)
            
        return tweets_collection
                
tweetDumper = DataDumper()
search_keyword = "iphone"
limit = 1

#let's download tweets from twitter :D
tweetDumper.dumpTweets(search_keyword, limit)